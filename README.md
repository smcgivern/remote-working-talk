A talk about remote working!

See the latest version at
[https://smcgivern.gitlab.io/talks/remote-working/](https://smcgivern.gitlab.io/talks/remote-working/).

The content of this repo have moved to [https://gitlab.com/smcgivern/talks](https://gitlab.com/smcgivern/talks).
